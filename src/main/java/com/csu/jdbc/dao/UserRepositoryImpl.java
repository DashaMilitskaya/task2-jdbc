package com.csu.jdbc.dao;

import com.csu.jdbc.schema.User;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UserRepositoryImpl implements UserRepository {

    String url = "jdbc:mariadb://localhost:3306/java";
    String username = "root";
    String password = "";

    static {

        try {
            Class.forName("org.mariadb.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public User getUserById(Integer id) {

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT last_name, first_name, id, address, city FROM Users WHERE id = ?");
            statement.setInt(1, id);
            var result = statement.executeQuery();

            while (result.next()) {
                return User
                        .builder()
                        .id(result.getInt("id"))
                        .lastName(result.getString("last_name"))
                        .firstName(result.getString("first_name"))
                        .address(result.getString("address"))
                        .city(result.getString("city"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserByFirstName(String name) {


        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT last_name, first_name, id, address, city FROM Users WHERE first_name = ?");
            statement.setString(1, name);
            var result = statement.executeQuery();

            while (result.next()) {
                return User
                        .builder()
                        .id(result.getInt("id"))
                        .lastName(result.getString("last_name"))
                        .firstName(result.getString("first_name"))
                        .address(result.getString("address"))
                        .city(result.getString("city"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getUserByLastName(String name) {

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT last_name, first_name, id, address, city FROM Users WHERE last_name = ?");
            statement.setString(1, name);
            var result = statement.executeQuery();

            while (result.next()) {
                return User
                        .builder()
                        .id(result.getInt("id"))
                        .lastName(result.getString("last_name"))
                        .firstName(result.getString("first_name"))
                        .address(result.getString("address"))
                        .city(result.getString("city"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void save(User user) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn
                    .prepareStatement("INSERT INTO users (last_name, first_name, address, city) VALUES (?, ?, ?, ?)");
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getAddress());
            statement.setString(4, user.getCity());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
