package com.csu.jdbc;

import com.csu.jdbc.dao.UserRepository;
import com.csu.jdbc.dao.UserRepositoryImpl;
import com.csu.jdbc.schema.User;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = new UserRepositoryImpl();
        System.out.println(userRepository.getUserById(1));
        System.out.println(userRepository.getUserByFirstName("Ivanov"));
        System.out.println(userRepository.getUserByLastName("Ivan"));
        /*
        userRepository.save(User
                .builder()
                        .lastName("Ivan")
                        .firstName("Ivanov")
                        .address("Street")
                        .city("Chelyabinsk")
                .build());*/

    }

}
